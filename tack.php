<!DOCTYPE html>
<html>
    <head>
        <title> Varukorg </title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="style.css">
    </head>
    <body>

         <?php 
            include "produkter.php";
            include "funktioner.php";
        ?> 


         <!-- #### HEADER BÖRJAR #### -->
    <div class="mitten">
        <header>
            <p class="logga">- v e g o m a c k a n -</1>
            <hr>
        </header>

        
            <h1 class="nedåt">Tack för din beställning!</h1>     

       
    
       

       <?php

       /* #####  LOOP FÖR UTRÄKNING AV DEL- OCH TOTALSUMMOR #####*/

            $totalsumma = 0;

            for($i=0; $i<count($produkter); $i++) {
                $smak = $produkter[$i][0];
                $pris = kollaPris($produkter[$i][1]);     
                $antal = $_POST["p-".$i];
                $delsumma = $pris * $antal;
                    if($antal <= "0" || $antal == ""){   // Hoppar över ej ifylld ruta eller ruta med antalet 0
                        continue;
                    }

        /* ##### SKRIVER UT BESTÄLLNINGEN MED ANTAL OCH PRISER ##### */

                echo  $antal . " st. " . $smak . " à " . $pris . " kr " . "totalt: " . $delsumma . "  kr" . "</p>";
                $totalsumma += $delsumma;
             }

                echo "<hr>";
                echo "<p class='center'> Totalpris: " .  "<span class='pris'>" . $totalsumma . "</span>" . " kr</p>";


        // ##### HÄR DRAS SPECIALRABATTEN (om jämn dag, udda vecka, mellan kl. 13 - 17 -> 5% rabatt )#####

                if(date('j')%2 == 0 && date("W")%2 != 0 && date("G") > 13 &&  date("G") < 17){  
                        $nyTotal = $totalsumma * 0.95;
                        echo " <p class='röd center'> Ditt pris efter 5% avdrag: " .  "<span class='pris'>" . $nyTotal . "</span>" . " kr </p>"; 
                }   
                
                echo "<hr>";
        ?>        

         <!-- #### FORMULÄR MED LEVERANSUPPGIFTER ####-->
       
         <aside class="adressbox">
            <div class="center">
                <p class="grått12 nedåt">LEVERANSADRESS:</p>
                <form method="post" action="leverans.php">
                    <input class="kort" type="text" name="fornamn" placeholder="Förnamn"/>
                    <input class="kort" type="text" name="efternamn" placeholder="Efternamn"/><br>
                    <input class="lång" type="text" name="gatuadress" placeholder="Gatuadress"/><br>
                    <input type="number" name="postnr" placeholder="Postnummer"/>
                    <input type="text" name="postadr" placeholder="Ort"/><br>
                    <input type="tel" name="tfn" placeholder="Telefon"/>
                    <input type="email" name="e_mail" placeholder="E-mail"/><br>
                    <div class="knappbox">                    
                       <input class ="knapp" type="submit" name="submit" value="Nästa">
                    </div>   
                </form>
            </div>
        </aside>
        
        <hr>

        
        </div>
    </body>

</html>


