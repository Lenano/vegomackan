<!DOCTYPE html>
<html>
    <head>
        <title> Leverans </title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="style.css">
    </head>
    <body>


         <!-- #### HEADER BÖRJAR #### -->
        <div class="mitten">

            <?php
            
            if ($_POST["tfn"] == "") {
                echo "Du har glömt att fylla i ditt telefonnummer!";
                exit;
            }
        ?>

            <header>
                <p class="logga">- v e g o m a c k a n -</1>
                <hr>
            </header>
 

             <h1 class="nedåt">Din leverans:</h1>

             <!-- ##### BERÄKNAR LEVERANSTIDPUNKT m.m ##### -->

        <?php        
                    if(date('j')%2 == 0 && date('l')== "Monday") {
                        echo "Dina varor beräknas levereras imorgon.<br>
                        Du får 10 kronors rabatt.";
                    }
                    elseif(date('j')%2 != 0 && date('l')== 'Wednesday') {
                        echo "Dina varor beräknas levereras inom två arbetsdagar.<br>
                        Extra kostnad: 20 kr.";
                    }
                    elseif(date('W')%2 == 0 && date('l') == 'Thursday') {
                        echo "Dina varor beräknas levereras inom en vecka.";
                    }
                    elseif(date('W')%2 != 0 && date('l') == 'Sunday') {
                        echo "Dina varor beräknas levereras om fem timmar.";
                    } 
                    else {
                        echo "Dina varor beräknas levereras inom tre dagar. ";
                    }
                    
        ?>
            
            <hr>
            
            <!-- ###### LEVERANSADRESS ######-->

        <?php
                    echo "<br>" . "<span class='grått12'> DU HAR UPPGETT FÖLJANDE LEVERANSADRESS: </span>" . "<br>" .
                    $_POST['fornamn'] . " " . $_POST['efternamn']."<br>".
                    $_POST['gatuadress'] ."<br>" .
                    $_POST['postnr'] . " " . $_POST['postadr'] . "<br>"; 
                    
        ?>
            <div class="nedåt"></div>
            <hr>
            <p> Tack för att du beställde från vegomackan. <br> Välkommen åter!</p>
    
        </div>    
    </body>

</html>
