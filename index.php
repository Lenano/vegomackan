<!DOCTYPE html>
<html>
    <head>
        <title>Startsida</title>
        <meta charset="utf-8">
        <link rel="stylesheet" href="style.css">
    </head>
    <body>

    <!-- #### ARRAYER MED SMAK OCH PRIS #### -->

    <?php
       include "produkter.php";
       include "funktioner.php"; 
    ?>

    <!-- #### HEADER BÖRJAR #### -->
    <div class="mitten">
        <header>
            <p class="logga">- v e g o m a c k a n -</1>
            <hr>
        </header>

        <main> 

    <!-- #### TABELL MED BESTÄLLNINGSSORTIMENT #### -->

            <section class="nedåt"> 
                <h1> Våra mackor: </h1>
                <form method="post" action="tack.php">
                    <table>
                        <tr colspan=3>
                            <th>PÅLÄGG</th>
                            <th>PRIS</th>
                            <th>ANTAL</th>
                        </tr>
                        <?php
                            for($i=0; $i<count($produkter); $i++) {
                        ?>
                        <tr>
                            <td class="produkter"> <?php echo $produkter[$i][0]; ?> </td>
                            <td class="pris"> <?php echo kollaPris($produkter[$i][1]); ?>  <span class="normal"> kr <span> </td>
                            <td class="antal"> <input class="antal" type="number" name="p-<?php echo $i; ?>"> st. </td>        
                        </tr>
                        <?php
                            }
                        ?>
                    </table>

                <!-- #### KNAPP SUBMIT #### -->
                    <div class="knappbox">
                        <input class="knapp" type="submit" name="submit" value="Beställ">
                    </div>
                </form>

                <?php

                // ##### MEDDELANDE OM SPECIALRABATTEN (om jämn dag, udda vecka, mellan kl. 13 - 17 -> 5% rabatt )#####

                if(date('j')%2 == 0 && date("W")%2 != 0 && date("G") > 13 &&  date("G") < 17){
                        echo "<p class='röd center'>Just nu får du 5% rabatt på hela din beställning! <br> 
                        Dessutom levereras dina varor redan imorgon. Rabatten dras på nästa sida.<p>"; 
                }   
                ?>
            </section>
            <hr>

        </main>
        

        <!-- #### DATUM OCH TID ####-->
        <div class="datum center">
        <?php
           setlocale(LC_ALL, "sv_SE");
           echo lcfirst(strftime("%A")) . " den " . date("j") . " " . lcfirst(strftime("%B")) . " " . date("Y");    
        ?>
        </div>
    </div>
    
    </body>

</html>
